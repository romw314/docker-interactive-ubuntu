# Docker Interactive Ubuntu (`rind`)

Work and test apps in an Ubuntu environment running in Docker.

## Prerequisites

Linux, with `sh` (`busybox sh` is enough), Docker and cURL or Wget (`busybox wget` is enough) installed.

## Installation

Use the install script.

With cURL:

```sh
curl -qo- https://gitlab.com/romw314/docker-interactive-ubuntu/-/raw/master/install.sh
```

or with Wget:

```sh
wget -qO- https://gitlab.com/romw314/docker-interactive-ubuntu/-/raw/master/install.sh
```

You need to have `~/.local/bin` in your `PATH`.

## Usage

In the directory you want to work in, run:

```sh
rind
```
