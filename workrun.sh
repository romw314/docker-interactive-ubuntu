#!/bin/sh
docker run --workdir /dtu/workspace -v .:/dtu/workspace -e DTU_WORKSPACE=/dtu/workspace -e CI=false -e DTU=true -e TERM="$TERM" -e ROMW314_INTERACTIVE=true -e NODE_ENV=development -it --rm romw314/interactive "$@"
