#!/bin/sh

##########################################
readonly base_url="https://gitlab.com/romw314/docker-interactive-ubuntu/-/raw/master"
##########################################

echo -n "Checking for docker... "
if ! type docker; then
	echo "You need to have Docker installed."
fi

echo -n "Checking for curl... "
if type curl; then
	download() {
		curl -qo"$2" -- "$1"
	}
else
	echo -n "Checking for wget... "
	if ! type wget; then
		echo "You need to have cURL or Wget installed."
		exit 1
	fi
	download() {
		wget -qO"$2" -- "$1"
	}
fi

mkdir -vp "$HOME"/.local/bin
download "$base_url"/workrun.sh "$HOME"/.local/bin/rind
chmod +x "$HOME"/.local/bin/rind
docker pull romw314/interactive

echo
echo "/----\\"
echo "| OK |"
echo "\\----/"
echo
