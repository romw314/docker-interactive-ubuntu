FROM ubuntu:focal

RUN apt-get update && apt-get install -y perl fish zsh sudo vim git wget

RUN adduser --home /dtu/home --shell /bin/bash --disabled-password --gecos 'Work User' work
COPY worksudo.sudoers /etc/sudoers.d/worksudo
RUN chown -R work:work /dtu

USER work:work
WORKDIR /dtu/workspace

COPY work.bashrc /dtu/home/

# NVM
ADD --chown=work:work --chmod=755 https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.7/install.sh /dtu/install-nvm.sh
RUN /dtu/install-nvm.sh
RUN bash -ic 'nvm install 20'
RUN bash -ic 'corepack enable'

# PNPM
RUN bash -ic 'SHELL=bash pnpm setup'
RUN bash -ic 'SHELL=zsh pnpm setup'

# JavaScript CLI tools
RUN bash -ic 'pnpm add -g npm-name jake yo gulp-cli grunt-cli'
