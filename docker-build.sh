#!/bin/sh

set -eu

######################
readonly owner="romw314" name="interactive"
######################

if echo "$1" | grep -q d; then
	DRYRUN=1
fi
run() {
	if [ "${DRYRUN:-}${DRY_RUN:-}${DDR:-}" ]; then
		echo ":: $*"
	else
		"$@"
	fi
}

run docker build -t "${owner:-}${owner:+/}$name" .
